import java.util.Arrays;
import java.util.Collections;

public class ColorSort{

   enum Color {red, green, blue};

   public static void main (String[] param) {
      // for debugging
   }

   public static void reorder (Color[] balls) {
      /* Inspired by Taltech student Eric Rodionov*/

      int[] colorArray = new int[3];
      for (Color color : balls){
         colorArray[color.ordinal()]++;
      }
      int redCount = colorArray[0];
      int greenCount = colorArray[1];
      int blueCount = colorArray[2];

      if (redCount != 0) {
         Arrays.fill(balls, 0, redCount, Color.red);
      }

      if (greenCount != 0) {
         Arrays.fill(balls, redCount, redCount + greenCount, Color.green);
      }

      if (blueCount != 0) {
         Arrays.fill(balls, greenCount + redCount, redCount + greenCount + blueCount, Color.blue);
      }
   }
}